module.exports = function(GoOrganization) {
  // This intersects the access operation in order to add additional filters
  // NOTE: This is not necessary anymore because the models can add additional filters
  // on their definition
  /*GoOrganization.observe('access', function logQuery(ctx, next) {
   ctx.query.include = ['goOrganizationType', 'goConference'];
   next();
   });*/

  GoOrganization.afterRemote('find', function(ctx, goOrganizations, next) {
    if (ctx.result) {
      ctx.result = {'Organizations': goOrganizations};
    }
    next();
  });

  GoOrganization.afterRemote('findById', function(ctx, goOrganization, next) {
    if (ctx.result) {
      ctx.result = {'Organization': goOrganization};
    }
    next();
  });
};
