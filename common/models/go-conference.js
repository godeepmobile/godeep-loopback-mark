module.exports = function(GoConference) {
  // This intersects the access operation in order to add additional filters
  // NOTE: This is not necessary anymore because the models can add additional filters
  // on their definition
  /*GoOrganization.observe('access', function logQuery(ctx, next) {
   ctx.query.include = ['goOrganizationType', 'goConference'];
   next();
   });*/

  GoConference.afterRemote('find', function(ctx, goConferences, next) {
    if (ctx.result) {
      ctx.result = {'Conferences': goConferences};
    }
    next();
  });

  GoConference.afterRemote('findById', function(ctx, goConference, next) {
    if (ctx.result) {
      ctx.result = {'Conference': goConference};
    }
    next();
  });
};
