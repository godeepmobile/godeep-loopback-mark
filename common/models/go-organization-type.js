module.exports = function(GoOrganizationType) {
  // This intersects the access operation in order to add additional filters
  // NOTE: This is not necessary anymore because the models can add additional filters
  // on their definition
  /*GoOrganization.observe('access', function logQuery(ctx, next) {
   ctx.query.include = ['goOrganizationType', 'goConference'];
   next();
   });*/

  GoOrganizationType.afterRemote('find', function(ctx, goOrganizationTypes, next) {
    if (ctx.result) {
      ctx.result = {'OrganizationTypes': goOrganizationTypes};
    }
    next();
  });

  GoOrganizationType.afterRemote('findById', function(ctx, goOrganizationType, next) {
    if (ctx.result) {
      ctx.result = {'OrganizationType': goOrganizationType};
    }
    next();
  });
};
